# Gez's Chuck Norris Jokes Service

I know this has been missing from all of your lives so I have wrapped calls to an API that provides Chuck Norris jokes in an AWS Lambda function and then exposed it with an Api-Gateway.

### Build and Deployment Instructions

\*As terraform comes as a small, single binary I have tracked it in source control so that the user does not need to have terraform installed

    npm install
    npm run package
    cd terraform
    ./terraform plan --out=plan.txt
    ./terraform apply "plan.txt"

Just make a GET Request and receive a random joke. I don't know where these jokes are populated from but it's a very mixed bag so BE WARNED :)

### Technology Used:

- Typescript
- Node.js
- webpack
- terraform
- AWS Lambda
- AWS Api-Gateway
- AWS IAM

### Known Limitations

- My focus has been almost solely on configuring the terraform modules such that I can deploy the lambda and expose it with an Api-Gateway so the code itself is really fairly straightforward. Focus was not on showing offtypescript skills as much as DEVOPS.
