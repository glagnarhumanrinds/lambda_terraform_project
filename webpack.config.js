const path = require("path");

module.exports = {
  entry: "./src/lambda.ts",
  target: "node",
  node: {
    fs: "empty",
    global: true
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: "ts-loader",
        exclude: /node_modules/
      }
    ]
  },
  resolve: {
    extensions: [".tsx", ".ts", ".js", ".json"]
  },
  output: {
    filename: "lambda.js",
    libraryTarget: "commonjs",
    path: path.resolve(__dirname, "dist")
  }
};
