import { get, post } from "request-promise"

export class JokeService {

    public fetchJoke(): any {
        const url = "https://api.chucknorris.io/jokes/random";
        return get(url)
    }
}
