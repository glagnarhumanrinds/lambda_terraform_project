export class HTTPResponse {
    
    isBase64Encoded: boolean;
    statusCode: Number;
    headers: Object;
    body: any;

    constructor(code: Number, body: any) {
        this.isBase64Encoded = true;
        this.statusCode = code;
        this.headers = {};
        this.body = body;
    }
}