import { APIGatewayEvent } from "aws-lambda";
import { JokeService } from "../../service/JokeService";
import { HTTPResponse } from "../../models/response/HTTPResponse";

exports.handler = async (event: APIGatewayEvent): Promise<any> => {
  const jokes = new JokeService();
    
  return jokes
    .fetchJoke()
    .then(JSON.parse)
    .then((joke: any) => joke.value)
    .then((joke: String) => new HTTPResponse(200, joke))
    .catch((err: Error) => new HTTPResponse(500, err.message));
};
