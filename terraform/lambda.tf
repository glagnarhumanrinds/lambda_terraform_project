resource "aws_lambda_function" "lambda" {
    function_name = "lambda"
    handler = "lambda.handler"
    runtime = "nodejs10.x"
    filename = "../dist/lambda.zip"
    source_code_hash = "${base64sha256(filebase64("../dist/lambda.zip"))}"
    role = "${aws_iam_role.lambda_exec_role.arn}"
}

resource "aws_lambda_permission" "apigw_lambda" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.lambda.function_name}"
  principal     = "apigateway.amazonaws.com"

  source_arn = "arn:aws:execute-api:${var.myregion}:${var.accountId}:${aws_api_gateway_rest_api.lambda_api.id}/*/${aws_api_gateway_method.lambda_api.http_method}${aws_api_gateway_resource.lambda_api.path}"
}