resource "aws_api_gateway_stage" "lambda_api" {
  stage_name    = "prod"
  rest_api_id   = "${aws_api_gateway_rest_api.lambda_api.id}"
  deployment_id = "${aws_api_gateway_deployment.lambda_api.id}"
}

resource "aws_api_gateway_deployment" "lambda_api" {
  depends_on  = ["aws_api_gateway_integration.integration"]
  rest_api_id = "${aws_api_gateway_rest_api.lambda_api.id}"
  stage_name  = "dev"
}
resource "aws_api_gateway_rest_api" "lambda_api" {
  name = "Gez's Chuck Norris API"
}

resource "aws_api_gateway_resource" "lambda_api" {
  path_part   = "resource"
  parent_id   = "${aws_api_gateway_rest_api.lambda_api.root_resource_id}"
  rest_api_id = "${aws_api_gateway_rest_api.lambda_api.id}"
}

resource "aws_api_gateway_method" "lambda_api" {
  rest_api_id   = "${aws_api_gateway_rest_api.lambda_api.id}"
  resource_id   = "${aws_api_gateway_resource.lambda_api.id}"
  http_method   = "GET"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "integration" {
  rest_api_id             = "${aws_api_gateway_rest_api.lambda_api.id}"
  resource_id             = "${aws_api_gateway_resource.lambda_api.id}"
  http_method             = "${aws_api_gateway_method.lambda_api.http_method}"
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = "${aws_lambda_function.lambda.invoke_arn}"
}